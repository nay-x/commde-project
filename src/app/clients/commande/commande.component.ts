import { Component, OnInit } from '@angular/core';
import {RestApiService} from '../../rest-api.service';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {
  
  id=this.actRoute.snapshot.params['id'];
  commandeData:any={};
  Produits:any=[];
  constructor(public restApi:RestApiService,public actRoute:ActivatedRoute,public router:Router) { }

  ngOnInit() {
    this.loadProduits();
  }

  loadProduits(){
    return this.restApi.getProducts()
      .subscribe((data:{})=>{
        this.Produits=data;
        console.log(data);
      })
  }

  commandProduct(){
    //create command
    //send command product
    
  }

}

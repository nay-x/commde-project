import { Component, OnInit } from '@angular/core';
import {RestApiService} from '../../rest-api.service';
import {ActivatedRoute,Router} from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  form2 = new FormGroup({
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    age: new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(2),

    ]),
    telephone: new FormControl('', 
     Validators.required),
   });
   
  id=this.actRoute.snapshot.params['id'];
  clientData:any={};
  constructor(public restApi:RestApiService,public actRoute:ActivatedRoute,public router:Router) { }

  ngOnInit() {
    this.restApi.getClient(this.id)
      .subscribe((data:{})=>{
        this.clientData = data;
      })
  }

  updateClient(){
    if(window.confirm('Are you sure, you want to update client?')){
      this.restApi.updateClient(this.id,this.clientData)
        .subscribe(data=>{
          this.router.navigate(['/list-client'])
        })
    }
  }

}

import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
import {RestApiService} from '../../rest-api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    form = new FormGroup({
    nom: new FormControl('', Validators.required),
    prenom: new FormControl('', Validators.required),
    age: new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(2), ]),
    telephone: new FormControl('', Validators.required),
   });

  @Input() clientData = {nom_Client:'',prenom_Client:'',age:0,tel:''}
  constructor(public restApi:RestApiService,public router:Router) { }

  ngOnInit() {
  }
 
  public addClient(dataClient){
    this.restApi.createClient(this.clientData)
      .subscribe((data:{})=>{
        this.router.navigate(['/list-client'])
      });
  }

}

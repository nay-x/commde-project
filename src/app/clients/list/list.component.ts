import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {RestApiService} from '../../rest-api.service';
import {Client} from '../../client';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  Client:any=[];

  constructor(public restApi:RestApiService) { }

  ngOnInit() {
    this.loadClients();
  }

  //get All Clients
  loadClients(){
    return this.restApi.getClients()
      .subscribe((data:{})=>{
        this.Client=data;
        console.log(data);
      })
      
  }

  deleteClient(id){
    if(window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteClient(id)
        .subscribe(data => {
          this.loadClients()
        })
    }
  }

}

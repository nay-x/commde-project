import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {CreateComponent} from './clients/create/create.component';
import {UpdateComponent} from './clients/update/update.component';
import {ListComponent} from './clients/list/list.component';
import { CommandeComponent } from './clients/commande/commande.component';

const routes:Routes = [
  {path :'', pathMatch:'full',redirectTo:'list-client'},
  {path:'create-client',component:CreateComponent},
  {path:'list-client', component:ListComponent},
  {path:'update-client/:id',component:UpdateComponent},
  {path:'commande/:id',component:CommandeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

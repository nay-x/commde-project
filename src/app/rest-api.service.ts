import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {Client} from './client';
import {Produit} from './produit';
import {Observable,throwError, forkJoin} from 'rxjs';
import {retry,catchError} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = 'http://localhost:80/command/apiserver';
  constructor(private http:HttpClient) { }

  /*==========================================
  CRUD Method for Client
  ==========================================*/

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' :'application/json'
    })
  }

  // API Get() method => fetch Clients list
  getClients():Observable<Client>{
    return this.http.get<Client>(this.apiURL+'/client')
     .pipe(
       retry(1),
       catchError(this.handleError)
     )
  }

  // API Get() Client by ID
  getClient(id):Observable<Client>{
    return this.http.get<Client>(this.apiURL+'/client/'+id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  //API Post() create Client
  createClient(client):Observable<Client>{
    return this.http.post<Client>(this.apiURL+'/client',JSON.stringify(client),this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  //API PUT() update client
  updateClient(id,client):Observable<Client>{
    return this.http.put<Client>(this.apiURL+'/client/'+id,JSON.stringify(client),this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  //API DELETE() delete client 
  deleteClient(id){
    return this.http.delete<Client>(this.apiURL + '/client/' +id ,this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  //Error Handling
  handleError(error){
    let errorMessage="";
    if(error.error instanceof ErrorEvent){
      //Get Client-side Error
      errorMessage = error.error.message;
    }else{
      //get Server-side Error
      errorMessage = `Error Code :${error.status}\nMessage:${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }


  // API GET() Products
  getProducts():Observable<Produit>{
    return this.http.get<Produit>(this.apiURL+'/produit')
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  //API POST Commande
  /*public postCommands(date,num,clientid):Observable<Any[]>{
    let response1 = this.http.post(this.apiURL+'commande'+date+num,clientid); 
    return forkJoin([response1]);
  }*/






}
